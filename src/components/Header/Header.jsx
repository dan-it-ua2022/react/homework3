import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.module.scss';

import { HeaderMenu } from '../../UI/Navigation/HeaderMenu/HeaderMenu';
import { Logo } from '../../UI/Logo/Logo';
import { StarIco } from '../../UI/Icons/StarIco/StarIco';
import { BasketIco } from '../../UI/Icons/BasketIco/BasketIco';

export const Header = ({ favorites, productsInBasket }) => {
  const countFavorites = favorites.length;
  const countProductsInBasket = productsInBasket.length;

  return (
    <div className={styles.Header__row}>
      <Logo />
      <HeaderMenu />

      <div className={styles.HeaderActions}>
        <div className={styles.HeaderBasket}>
          <BasketIco width={26} fill={'#1c8646'} />
          <span className={styles.basketValue}>{countProductsInBasket}</span>
        </div>
        <div className={styles.HeaderFavorites}>
          <StarIco width={26} fill={'#ffda12'} />
          <span className={styles.favoritesValue}>{countFavorites}</span>
        </div>
      </div>
    </div>
  );
};

Header.propTypes = {
  favorites: PropTypes.array.isRequired,
  productsInBasket: PropTypes.array.isRequired,
};
